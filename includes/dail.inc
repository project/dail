<?php

/**
 * @file: dail.inc
 *
 * The base AI class.
 * TODO: Make this useful. ;)
 */

class dail {

  /**
   * A logging object for this dail instance.
   * TODO:
   */
  protected $logger = array();

  function __construct($collection = 'default') {
    $this->type = 'dail';
    $this->collection = $collection;
  }

  function collection() {
    return $this->collection;
  }
}

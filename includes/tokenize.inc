<?php

/**
 * @file: token.inc
 *
 * A tokenizer class.
 */

class Tokenizer {

  public $tokens = array();

  private $prepend_key = FALSE;
  private $prepend_delimiter = '.';

  private $sanitize = TRUE;
  private $force_lowercase = TRUE;

  private $delimiters = " \t\n\r-_<>'\"`/|*%^&+=~:;?";
  private $strip_patterns = array(
    '(http://|https://ftp://|mailto://)',
    "'(www\.)|(</a>)|(href=)|(target=)|(src=)'i",
    '/[()\{\}\[\]#.,]/');
  // TODO: replace_patterns

  /**
   * @array $options
   *   The following options can be passed in to affect tokenization:
   *     prepend_key: whether to prepend the array key to the tokens
   *     delimiter: an optional delimiter between the prepend key and token
   */
  function __construct($strings = array(), $options = array()) {
    if (is_array($options)) {
      if (isset($options['prepend_key'])) {
        $this->prepend_key = $options['prepend_key'];
      }
      if (isset($options['prepend_delimiter']) && is_string($options['prepend_delimiter'])) {
        $this->prepend_delimiter = $options['prepend_delimiter'];
      }
      if (isset($options['force_lowercase'])) {
        $this->force_lowercase = $options['force_lowercase'];
      }
      if (isset($options['sanitize'])) {
        $this->sanitize = $options['sanitize'];
      }
    }
    $this->clear();
    if (!empty($strings)) {
      $this->tokenize($strings);
    }
  }

  /**
   * @array $strings
   *   One or more strings in array, to be split into tokens.
   */
  function tokenize($strings = array()) {
    foreach ($strings as $key => $string) {
      $sanitized = $string;
      if ($this->force_lowercase) {
        // By forcing all tokens to lowercase, we aggregate tokens together.
        // This aims to lower the total number of tokens, and increase the
        // strength of individual tokens.
        $sanitized = drupal_strtolower($sanitized);
      }
      if ($this->sanitize) {
        // Strip out values that should not be considered part part of tokens,
        // so things like '{viagra}' and 'vi.agra' become simply 'viagra'.
        $sanitized = preg_replace($this->strip_patterns, '', $sanitized);
      }
      $tok = strtok($sanitized, $this->delimiters);
      while ($tok !== FALSE) {
        if ($this->prepend_key === TRUE) {
          $tok = $key . $this->prepend_delimiter . $tok;
        }
        $this->tokens[]= $tok;
        $tok = strtok($this->delimiters);
      }
    }
  }

  /**
   * Deletes all tokens contained in the current Bayesian object.
   */
  function clear() {
    $this->tokens = array();
  }

  /**
   * @string $delimiters
   *  Optional string to set custom delimiters for tokenizing a string.
   * @return
   *  The current delimiters string.
   */
  function delimiters($delimiters = NULL) {
    if (isset($delimiters) && is_string($delimiters)) {
      $this->delimiters = $delimiters;
    }
    return $this->delimiters;
  }

  /**
   * @array $strip_patterns
   *  Optional array to set custom strip_patterns for sanitizing strings.
   * @return
   *  The current strip_patterns array.
   */
  function strip_patterns($patterns = array()) {
    if (is_array($patterns) && !empty($patterns)) {
      $this->strip_patterns = $patterns;
    }
    return $this->strip_patterns;
  }
}
